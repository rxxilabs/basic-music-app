angular.module('app')
	.controller('musicController', function($scope, musicService, playlistService) {
		
		musicService.getMusic().success(function(response) {
			$scope.music = response;
		});
		
		$scope.addToPlaylist = function(song) {
			playlistService.addSong(song);
		};
		
	});