angular.module('app')
	.controller('playlistController', function($scope, $rootScope, playlistService) {
		
		$scope.playlist = [];
		
		playlistService.getPlaylist();
		
		$scope.$on('playlist.get', function(event, items) {
			$scope.playlist = items;
		});
		
		$scope.removeSong = function(index) {
			$rootScope.$broadcast('playlist.remove', index);
		};
		
		$scope.playSong = function(song, index) {
			$rootScope.$broadcast('playlist.play', song, index);
		};
		
	});
	