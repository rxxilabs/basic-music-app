angular.module('app')
	.controller('playerController', function($scope, playlistService) {
		
		$scope.songMetaViewable = false;
		$scope.playing = false;
		$scope.song = playlistService.currentSong;
		$scope.percentagePlayed = 0;
		$scope.songSecondsPlayed = 0;
		$scope.progress = undefined;
		
		$scope.pauseSong = function() {
			$scope.playing = false;
		};
		
		$scope.playSong = function() {
			if ($scope.songMetaViewable === false) return;
			
			$scope.playing = true;
			$scope.mockPlayer($scope.songSecondsPlayed, $scope.song.duration);
		};
		
		$scope.playNextSong = function() {
			if ($scope.songMetaViewable === false) return;
			
			$scope.song = playlistService.getNextSong();
		};
		
		$scope.playPrevSong = function() {
			if ($scope.songMetaViewable === false) return;
			
			$scope.song = playlistService.getPrevSong();
		};
		
		// Subscriptions
		$scope.$on('playlist.play', function( event, song ) {
			$scope.songMetaViewable = true;
			$scope.playing = true;
			$scope.song = song;
			
			$scope.mockPlayer(0, song.duration);
			
		});
		
		var thisScope = $scope;
		
		$scope.$on('playlist.remove', function(event, index) {
			if ( $scope.song !== null && playlistService.currentSong !== null && $scope.song.id === playlistService.playlist[index].song.id) {
				
				$scope.songMetaViewable = false;
				$scope.playing = false;
				
			}
		});
		
		$scope.mockPlayer = function(currentTime, duration) {
			if ($scope.playing === false) return;
			
			if (currentTime === 0)
				clearTimeout( $scope.progress );
			
		    $scope.progress = setTimeout(function() {
				var newtime = currentTime + 1;

				$scope.$apply(function() {
					$scope.songSecondsPlayed = currentTime;
					$scope.percentagePlayed = (currentTime/duration*100).toFixed(2);
				});
			   
				if ( newtime < (duration+1)) {
					$scope.mockPlayer(newtime, duration);
				}
			   
		   }, 1000);

		};
		
	});