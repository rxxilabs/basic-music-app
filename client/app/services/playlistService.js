angular.module('app')
.service('playlistService', function($http, $rootScope) {
	
	var base = this;
	
	this.playlist = [];
	this.currentSong = null;
	this.currentSongIndex = null;
	
	this.addSong = function(song) {
		$http.post("http://localhost:3000/playlist", { songId: song.id })
		.success(function(data, status) {
			base.playlist.push({ songId: song.id, song: song });
		});
		
	};
	
	this.getPlaylist = function() {
		$http.get("http://localhost:3000/playlist?_expand=song").success(function(response) {
			base.playlist = response;
			$rootScope.$broadcast('playlist.get', response);
		});
	};
	
	this.getNextSong = function() {
		var totalSongsInPlaylist = base.playlist.length;
		if (totalSongsInPlaylist > 1 && base.currentSongIndex !== (totalSongsInPlaylist - 1)) {
			base.currentSongIndex++;
			base.currentSong = base.playlist[base.currentSongIndex];
		}
		return base.playlist[base.currentSongIndex].song;
	};
	
	this.getPrevSong = function() {
		var totalSongsInPlaylist = base.playlist.length;
		
		if (totalSongsInPlaylist > 1 && base.currentSongIndex !== 0) {
			base.currentSongIndex--;
			base.currentSong = base.playlist[base.currentSongIndex];
		}
		
		return base.playlist[base.currentSongIndex].song;
		
	};
	
	// Subscriptions
	$rootScope.$on('playlist.play', function(event, song, index) {
		base.currentSong = song;
		base.currentSongIndex = index;
	});
	
	$rootScope.$on('playlist.remove', function(event, index) {
		var song = base.playlist[index];
		
		$http.delete("http://localhost:3000/playlist/"+song.id).success(function() {
			if (base.currentSongIndex === index) {
				base.currentSong = null;
				base.currentSongIndex = null;
			}
			
			base.playlist.splice(index, 1);
		});
	});
	
});