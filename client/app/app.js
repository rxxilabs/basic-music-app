angular.module('app', [
	'ui.router'
]);

angular.module('app')
	.controller('searchController', function($rootScope) {
		$rootScope.global = {
			musicSearch: ''
		};
		
		$rootScope.Math = window.Math;
		
		$rootScope.getDurationMinutes = function(seconds) {
			return Math.floor(seconds/60);
		};
		
		$rootScope.getDurationSeconds = function(seconds) {
			var num = Math.ceil((Math.floor(seconds/60) - seconds/60)*-60);
			
			if( num.toString().length < 2 )
				return "0" + num;
				
			return num.toString();
		};
	});

angular.module('app')
	.config(function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('');
	
	$stateProvider
		.state('index', {
			url: '',
			views:  {
				'player': {
					templateUrl: 'app/templates/player.html',
					controller: 'playerController'
				},
				'playlist': {
					templateUrl: 'app/templates/playlist.html',
					controller: 'playlistController'
				},
				'music': {
					templateUrl: 'app/templates/music.html',
					controller: 'musicController'
				}
			}
		});
	});