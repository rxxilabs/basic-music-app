describe('music app', function() {
	
	beforeEach(function() {
		browser().navigateTo('http://localhost:8080');
	});

	it("should load playlist on page load", function() {
		var numberOfSongsInPlaylist = document.querySelectorAll('.music-items').length;
		
		expect(numberOfSongsInPlaylist > 0).toBe(true);
	});
  
});