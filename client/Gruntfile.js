module.exports = function( grunt ) {
	
	grunt.initConfig({
		jshint: {
		  build: [
			'Gruntfile.js', 
			'app/**/*.js',
			'!app/lib/**/*.js'
			]
		}
	});
	
	grunt.loadNpmTasks('grunt-contrib-jshint');
	
	grunt.registerTask('default', ['jshint']);
	
};