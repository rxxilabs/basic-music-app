Music App Testing With Angular
--------------------------------

Music App - Partially completed

### Setup

Run the json server within the **server** folder. I stole this from the technical requirements doc.

	$ cd server
	
	$ npm install -g json-server
	
	$ json-server db.json
	
Run the client 

	$ cd client
	
	$ npm install http-server -g
	
	$ http-server
	
Then go to http://localhost:8080


### The Server

Simply used the json-server. For the server side building this fully - I would still go with Node JS, but using Express JS as the API, and have a document based database.




